(defproject ixm-repl "1.0.0-SNAPSHOT"
  :url "http://repo.or.cz/w/ixm-repl.git"
  :description "Interact with a tribe of IXM through a Clojure repl"
  :dependencies [[org.clojure/clojure "1.1.0"]
                 [org.clojure/clojure-contrib "1.0-SNAPSHOT"]]
  :dev-dependencies [[leiningen/lein-swank "1.2.0-SNAPSHOT"]])
