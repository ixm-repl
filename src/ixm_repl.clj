(ns #^{:author "Eric Schulte",
       :license "GPLV3",
       :doc "interact with an IXM tribe through a Clojure repl"}
  ixm-repl
  (:import (java.io InputStreamReader OutputStreamWriter BufferedReader)
           (clojure.lang LineNumberingPushbackReader))
  (:use [clojure.main :only (repl)]
        clojure.contrib.duck-streams
        clojure.contrib.command-line
        clojure.contrib.except
        clojure.contrib.repl-utils))

(def tribe {:sfbprog-cmd "sfbprog"
            :usb-dev "/dev/ttyUSB0"})

(def repl-out *out*)

(def tribe-in nil)

(defn repl-print [line]
  (binding [*out* repl-out] (println line)))

(defn do-log [file] (fn [line] (append-spit file (str line "\n"))))

(defn tribe-print [line]
  (when (nil? tribe-in) (throwf "not connected to tribe"))
  (binding [*out* tribe-in] (println line)))

(defn $ [l] (tribe-print l))

(defn dump [line])

(def reflexes {:L repl-print})

(defn add-reflex [key func]
  (def reflexes (assoc reflexes key func)))

(defn react [line]
  ((get reflexes (keyword (subs line 0 1)) repl-print) line))

(defn connect []
  (future
   (let [proc (.exec (Runtime/getRuntime)
                     (into-array
                      (list (:sfbprog-cmd tribe)
                            "-n" (:usb-dev tribe) "-S" "-" "-t" "0")))]
     (with-open [stdout (BufferedReader. (InputStreamReader. (.getInputStream proc)))
                 proc-input (OutputStreamWriter. (.getOutputStream proc))]
       (def tribe-in proc-input)
       (binding [*in* stdout]
         (loop [] (when-let [line (read-line)] (react line) (recur))))))))
